package com.capgemini.jstk.boardgamecapmates.service.impl;

import com.capgemini.jstk.boardgamecapmates.commons.*;
import com.capgemini.jstk.boardgamecapmates.dao.impl.UserDao;
import com.capgemini.jstk.boardgamecapmates.enums.UserInformationType;
import com.capgemini.jstk.boardgamecapmates.mapper.UserMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ManageUserProfileServiceTest {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserMapper mapper;

    private ManageUserProfileService manageUserProfileService;

    @Before
    public void setUp()  {
        manageUserProfileService = new ManageUserProfileService(userDao, mapper);
    }

    @Test
    public void exceptionWhenUserNotFoundById(){
        String message = "";
        try {
            manageUserProfileService.getUserDataById(Long.valueOf(100));
        }
        catch (UserNotFoundException ex){
            message = ex.getMessage();
        }
        assertThat(message, is(equalTo("could not find user '100'.")));
    }

    @Test
    public void exceptionWhenUserNotFoundByDto(){
        UserDto userDto = buildUserDto(100L);
        String message = "";
        try {
            manageUserProfileService.getUserInformation(userDto, UserInformationType.LifeMotto);
        }
        catch (UserNotFoundException ex){
            message = ex.getMessage();
        }
        assertThat(message, is(equalTo("could not find user '100'.")));
    }

    @Test(expected = EmptyNameException.class)
    public void shouldExceptionWhenSetNameEmpty(){
        UserDto userDto = buildUserDto(1L);

        manageUserProfileService.setUserInformation(userDto, UserInformationType.FirstName, "");
    }

    @Test(expected = ToShortPasswordException.class)
    public void shouldExceptionWhenSetToShortPassword(){
        UserDto userDto = buildUserDto(1L);

        manageUserProfileService.setUserInformation(userDto, UserInformationType.Password, "1234567");
    }

    @Test(expected = NotValidEmailAddressException.class)
    public void shouldExceptionWhenSetIsNotEmailAddress(){
        UserDto userDto = buildUserDto(1L);

        manageUserProfileService.setUserInformation(userDto, UserInformationType.Email, "ala.pl");
    }

    @Test(expected = ToLongLifeMottoException.class)
    public void shouldExceptionWhenSetToLongLifeMotto(){
        UserDto userDto = buildUserDto(1L);

        manageUserProfileService.setUserInformation(userDto, UserInformationType.LifeMotto, "0123456789" +
                "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" +
                "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" +
                "012345678901234567890123456789");
    }


    @Test
    public void shouldGetUserInformation() {
        //given
        UserDto userDto = buildUserDto(1L);

        //when
        String firstName = manageUserProfileService.getUserInformation(userDto, UserInformationType.FirstName);
        String lastName = manageUserProfileService.getUserInformation(userDto, UserInformationType.LastName);
        String email = manageUserProfileService.getUserInformation(userDto, UserInformationType.Email);
        String password = manageUserProfileService.getUserInformation(userDto, UserInformationType.Password);

        //then
        assertThat(firstName, is(equalTo("fn2")));
        assertThat(lastName, is(equalTo("ln2")));
        assertThat(email, is(equalTo("e@2.pl")));
        assertThat(password, is(equalTo("p2")));
    }

    @Test
    public void shouldSetUserInformation() {
        //given
        UserDto userDto = buildUserDto(1L);

        //when
        manageUserProfileService.setUserInformation(userDto, UserInformationType.FirstName, "newFn");
        manageUserProfileService.setUserInformation(userDto, UserInformationType.LastName, "newLn");
        manageUserProfileService.setUserInformation(userDto, UserInformationType.Email, "newEmail@o2.pl");
        manageUserProfileService.setUserInformation(userDto, UserInformationType.Password, "newPasswd");

        //then
        userDto = buildUserDto(1L);
        assertThat(manageUserProfileService.getUserInformation(userDto, UserInformationType.FirstName),
                is(equalTo("newFn")));
        assertThat(manageUserProfileService.getUserInformation(userDto, UserInformationType.LastName),
                is(equalTo("newLn")));
        assertThat(manageUserProfileService.getUserInformation(userDto, UserInformationType.Email),
                is(equalTo("newEmail@o2.pl")));
        assertThat(manageUserProfileService.getUserInformation(userDto, UserInformationType.Password),
                is(equalTo("newPasswd")));
    }

    private UserDto buildUserDto(Long id){
        Optional<UserDto> userDto = mapper.map(userDao.findById(1L));
        userDto.get().setId(id);
        return userDto.get();
    }
}