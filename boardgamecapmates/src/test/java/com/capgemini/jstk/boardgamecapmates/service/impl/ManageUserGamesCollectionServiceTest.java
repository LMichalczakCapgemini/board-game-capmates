package com.capgemini.jstk.boardgamecapmates.service.impl;

import com.capgemini.jstk.boardgamecapmates.commons.NotFoundBoardGameInUserListException;
import com.capgemini.jstk.boardgamecapmates.commons.UserHasAlreadyThatGame;
import com.capgemini.jstk.boardgamecapmates.dao.impl.BoardGameDao;
import com.capgemini.jstk.boardgamecapmates.dao.impl.UserDao;
import com.capgemini.jstk.boardgamecapmates.entity.BoardGame;
import com.capgemini.jstk.boardgamecapmates.mapper.BoardGameMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.UserMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.BoardGameDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedList;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ManageUserGamesCollectionServiceTest {

    @Autowired
    private UserDao userDao;

    @Autowired
    private BoardGameDao boardGameDao;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private BoardGameMapper boardGameMapper;

    private ManageUserGamesCollectionService manageUserGamesCollectionService;

    @Before
    public void setUp() {
        manageUserGamesCollectionService = new ManageUserGamesCollectionService(userDao,boardGameDao);
        boardGameDao = new BoardGameDao(boardGameMapper);
    }

    @Test //passed when run alone
    public void getGamesCollection() {
        UserDto userDto = buildUserDto(1L);

        LinkedList<BoardGame> boardGames = manageUserGamesCollectionService.getGamesCollection(userDto);

        assertThat(boardGames.get(0), is(equalTo(boardGameDao.findById(1L).get())));
        assertThat(boardGames.get(0), is(not(equalTo(boardGameDao.findById(0L).get()))));
        assertThat(boardGames.get(1), is(equalTo(boardGameDao.findById(3L).get())));
        assertThat(boardGames.size(), is(equalTo(2)));
    }

    @Test
    public void getEmptyGamesCollection() {
        UserDto userDto = buildUserDto(2L);

        LinkedList<BoardGame> boardGames = manageUserGamesCollectionService.getGamesCollection(userDto);

        assertThat(boardGames.isEmpty(), is(equalTo(true)));
    }

    @Test
    public void removeGameFromUserGamesCollection() {
        UserDto userDto = buildUserDto(1L);
        BoardGameDto boardGameDto = buildBoardGameDto(3L);


        manageUserGamesCollectionService.removeGameFromCollection(userDto,boardGameDto);

        userDto = buildUserDto(1L);
        assertThat(userDto.getBoardGameList().size(), is(equalTo(1)));
    }

    @Test(expected = NotFoundBoardGameInUserListException.class)
    public void failRemoveGameFromUserGamesCollection() {
        UserDto userDto = buildUserDto(3L);
        BoardGameDto boardGameDto = buildBoardGameDto(3L);

        manageUserGamesCollectionService.removeGameFromCollection(userDto,boardGameDto);
    }

    @Test
    public void addNewGameToCollectionAndSystem(){
        UserDto userDto = buildUserDto(0L);
        BoardGameDto newGameDto = buildGameBoardDto();

        manageUserGamesCollectionService.addGameToCollection(userDto, newGameDto);

        assertThat(boardGameDao.findById(10L).isPresent(), is(true));
        assertThat(userDao.findById(0L).get().getBoardGameList().contains(newGameDto.getId()), is(true));
    }

    @Test(expected = UserHasAlreadyThatGame.class)
    public void failAddGameExistingInUserCollection(){
        UserDto userDto = buildUserDto(1L);
        BoardGameDto existingGameDto = buildBoardGameDto(1L);

        manageUserGamesCollectionService.addGameToCollection(userDto, existingGameDto);
    }

    private UserDto buildUserDto(Long id){
        Optional<UserDto> userDto = userMapper.map(userDao.findById(id));
        return userDto.get();
    }

    private BoardGameDto buildBoardGameDto(Long id){
        Optional<BoardGameDto> boardGameDto = boardGameMapper.map(boardGameDao.findById(id));
        return boardGameDto.get();
    }

    private BoardGameDto buildGameBoardDto(){
        BoardGameDto boardGameDto = new BoardGameDto();
        boardGameDto.setId(10L);
        boardGameDto.setName("name");
        boardGameDto.setDescription("desc");
        return  boardGameDto;
    }

}