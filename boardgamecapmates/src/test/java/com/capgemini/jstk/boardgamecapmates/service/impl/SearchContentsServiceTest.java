package com.capgemini.jstk.boardgamecapmates.service.impl;

import com.capgemini.jstk.boardgamecapmates.dao.impl.ContestDao;
import com.capgemini.jstk.boardgamecapmates.mapper.ContestMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.BoardGameDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.SearchContestCriteria;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;


@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SearchContentsServiceTest {

    @Autowired
    private ContestDao contestDao;

    @Autowired
    ContestMapper contestMapper;

    private SearchContentsService service;

    @Before
    public void setUp() {
        service = new SearchContentsService(contestDao, contestMapper);
    }

    @Test
    public void filterSearchContestsSucces() {
        UserDto userDto = buildUserDto(1L);
        BoardGameDto boardGameDto = buildBoardGameDto(1L);
        SearchContestCriteria searchContestCriteria = new SearchContestCriteria(userDto, boardGameDto, userDto);

        List<ContestDto> search = service.filterSearchContests(searchContestCriteria);

        assertThat(search.size(), is(equalTo(2)));
    }

    @Test
    public void filterSearchContestsZero() {
        UserDto userDto = buildUserDto(4L);
        BoardGameDto boardGameDto = buildBoardGameDto(1L);
        SearchContestCriteria searchContestCriteria = new SearchContestCriteria(userDto, boardGameDto, userDto);

        List<ContestDto> search = service.filterSearchContests(searchContestCriteria);

        assertThat(search.size(), is(equalTo(0)));
    }

    @Test
    public void filterSearchContestsAll() {
        SearchContestCriteria searchContestCriteria = new SearchContestCriteria(null, null, null);

        List<ContestDto> search = service.filterSearchContests(searchContestCriteria);

        assertThat(search.size(), is(equalTo(10)));
    }

    private UserDto buildUserDto(Long id){
        UserDto userDto = new UserDto();
        userDto.setId(id);
        return userDto;
    }

    private BoardGameDto buildBoardGameDto(Long id){
        BoardGameDto boardGameDto = new BoardGameDto();
        boardGameDto.setId(id);
        return boardGameDto;
    }
}