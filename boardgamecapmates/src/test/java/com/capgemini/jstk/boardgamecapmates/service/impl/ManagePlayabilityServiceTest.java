package com.capgemini.jstk.boardgamecapmates.service.impl;

import com.capgemini.jstk.boardgamecapmates.commons.UserHasAlreadyThatAvailabilityDayException;
import com.capgemini.jstk.boardgamecapmates.dao.impl.AvailabilityDayDao;
import com.capgemini.jstk.boardgamecapmates.dao.impl.ContestDao;
import com.capgemini.jstk.boardgamecapmates.dao.impl.UserDao;
import com.capgemini.jstk.boardgamecapmates.mapper.UserMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.AvailabilityDayDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ManagePlayabilityServiceTest {

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AvailabilityDayDao availabilityDayDao;
    @Autowired
    private ContestDao contestDao;


    private ManagePlayabilityService managePlayabilityService;

    @Before
    public void setUp() throws Exception {
        managePlayabilityService = new ManagePlayabilityService(userDao, userMapper, availabilityDayDao, contestDao);
    }

    @Test //passed when run alone
    public void shouldGetUsersWithSameAvailability(){
        UserDto userDto = buildUserDto(3L);
        AvailabilityDayDto availabilityDayDto = new AvailabilityDayDto();
        availabilityDayDto.setId(10L);
        availabilityDayDto.setUserId(userDto.getId());
        availabilityDayDto.setDayOfWeek(DayOfWeek.THURSDAY);

        managePlayabilityService.addAvailabilityHours(userDto,availabilityDayDto);

        assertThat(userDao.findById(3L).get().getAvailabilityDayList().contains(10L), is(equalTo(true)));
    }

    @Test(expected = UserHasAlreadyThatAvailabilityDayException.class)
    public void shouldExceptionWhenDuplicateAvailabilityDay(){
        UserDto userDto = buildUserDto(3L);
        AvailabilityDayDto availabilityDayDto = new AvailabilityDayDto();
        availabilityDayDto.setId(10L);
        availabilityDayDto.setUserId(userDto.getId());
        availabilityDayDto.setDayOfWeek(DayOfWeek.FRIDAY);

        managePlayabilityService.addAvailabilityHours(userDto,availabilityDayDto);

    }

    @Test
    public void shouldAddAvailabilityDay(){
        UserDto userDto = buildUserDto(3L);

        List<UserDto> userDtoList = managePlayabilityService.getUsersWithSimilarPlayability(userDto);

        assertThat(userDtoList.size(), is(equalTo(1)));
    }

    @Test
    public void makeContestWithOtherPlayer(){
        UserDto userDto = buildUserDto(3L);

        managePlayabilityService.createContestUsersWithSimilarPlayability(userDto);

        assertThat(contestDao.findByUserId(4L).size(), is(equalTo(1)));
    }

    @Test
    public void removePlayabilityWithComment(){
        UserDto userDto = buildUserDto(3L);
        AvailabilityDayDto availabilityDayDto = new AvailabilityDayDto();
        availabilityDayDto.setId(10L);
        availabilityDayDto.setUserId(userDto.getId());
        userDto.getAvailabilityDayList().add(10L);
        userDao.save(userDto);

        managePlayabilityService.deletePeriodWithComment(userDto,availabilityDayDto,"deleted");

        assertThat(availabilityDayDao.findById(10L).get().getComment(), is(equalTo("deleted")));
        assertThat(availabilityDayDao.findById(10L).get().getStatus(), is(equalTo(false)));
        assertThat(availabilityDayDao.findById(10L).get().getUserId(), is(equalTo(3L)));
        assertThat(userDao.findById(3L).get().getAvailabilityDayList().contains(10L), is(equalTo(false)));



    }


    private UserDto buildUserDto(Long id){
        Optional<UserDto> UserDto = userMapper.map(userDao.findById(id));
        return UserDto.get();
    }
}