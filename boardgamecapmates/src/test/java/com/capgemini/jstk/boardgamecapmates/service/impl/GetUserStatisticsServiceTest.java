package com.capgemini.jstk.boardgamecapmates.service.impl;

import com.capgemini.jstk.boardgamecapmates.dao.impl.ContestDao;
import com.capgemini.jstk.boardgamecapmates.dao.impl.LevelDao;
import com.capgemini.jstk.boardgamecapmates.dao.impl.UserDao;
import com.capgemini.jstk.boardgamecapmates.enums.LevelType;
import com.capgemini.jstk.boardgamecapmates.mapper.ContestMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.LevelMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.UserMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.LevelDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import static org.hamcrest.Matchers.*;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class GetUserStatisticsServiceTest {

    @Autowired
    private ContestDao contestDao;
    @Autowired
    private ContestMapper contestMapper;
    @Autowired
    private LevelDao levelDao;
    @Autowired
    private LevelMapper levelMapper;
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserMapper userMapper;


    private GetUserStatisticsService getUserStatisticsService;

    @Before
    public void setUp() throws Exception {
        getUserStatisticsService = new GetUserStatisticsService(contestDao, contestMapper, levelDao, levelMapper,
                userDao, userMapper);
    }

    @Test
    public void getUserPosition(){
        UserDto userDto = buildUserDto(1L);

        Long userPosition = getUserStatisticsService.getUserRankingPosition(userDto);

        assertThat(userPosition, is(equalTo(2L)));
    }

    @Test
    public void getUserWon(){
        UserDto userDto = buildUserDto(1L);

        Long wonCount = getUserStatisticsService.getUserWon(userDto);

        assertThat(wonCount, is(equalTo(5L)));
    }

    @Test
    public void getUserLost(){
        UserDto userDto = buildUserDto(3L);

        Long lostCount = getUserStatisticsService.getUserLost(userDto);

        assertThat(lostCount, is(equalTo(2L)));
    }

    @Test
    public void getUserLevel(){
        UserDto userDto = buildUserDto(2L);

        LevelDto levelDto = getUserStatisticsService.getUserLevel(userDto);

        assertThat(levelDto.getLevelType(), is(equalTo(LevelType.Beginner)));
    }

    @Test
    public void getUserHistory(){
        UserDto userDto = buildUserDto(2L);

        List<ContestDto> contestDtoList = getUserStatisticsService.getUserContestHistory(userDto);

        assertThat(contestDtoList.size(), is(equalTo(7)));
        assertThat(contestDtoList.get(0).getWinner(), is(equalTo(1L)));
    }

    private UserDto buildUserDto(Long id){
        Optional<UserDto> userDto = userMapper.map(userDao.findById(id));
        return userDto.get();
    }
}