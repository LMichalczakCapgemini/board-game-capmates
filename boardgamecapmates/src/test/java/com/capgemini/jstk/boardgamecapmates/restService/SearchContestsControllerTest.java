package com.capgemini.jstk.boardgamecapmates.restService;

import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.SearchContestCriteria;
import com.capgemini.jstk.boardgamecapmates.service.impl.SearchContentsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SearchContestsControllerTest {

    private MockMvc mockMvc;

    @Mock
    private SearchContentsService searchContentsService;

    @InjectMocks
    private SearchContestsController searchContestsController;

    private List<ContestDto> contentRepo;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(searchContestsController)
                .build();

        contentRepo = resetContentRepo();

        given(searchContentsService.filterSearchContests(Mockito.any(SearchContestCriteria.class))).willReturn(contentRepo);
    }

    @Test
    public void searchContests() throws Exception {
        mockMvc.perform(post("/contests/search")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new SearchContestCriteria(null,null,null))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(1)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].id", is(1)));

        verify(searchContentsService, times(1)).filterSearchContests(Mockito.any(SearchContestCriteria.class));
        verifyNoMoreInteractions(searchContentsService);
    }

    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<ContestDto> resetContentRepo(){
        List<ContestDto> contestDtoList = new LinkedList<>();
        ContestDto contestDto = new ContestDto();
        contestDto.setId(1L);
        contestDtoList.add(contestDto);
        return contestDtoList;
    }
}