package com.capgemini.jstk.boardgamecapmates.restService;

import com.capgemini.jstk.boardgamecapmates.commons.UserNotFoundException;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import com.capgemini.jstk.boardgamecapmates.service.impl.ManageUserProfileService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;





@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserDataControllerTest {

    private MockMvc mockMvc;

    @Mock
    private ManageUserProfileService manageUserProfileService;

    @InjectMocks
    private UserDataController userDataController;

    private List<UserDto> userRepo;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(userDataController)
                .build();

        userRepo = resetUserRepo();

        given(manageUserProfileService.getUserDataById(Mockito.anyLong())).willReturn(userRepo.get(0));
        given(manageUserProfileService.removeUser(Mockito.any(UserDto.class))).willReturn(true);
    }

    @Test
    public void testGetUserById() throws Exception{
        mockMvc.perform(get("/user/get").param("userId", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id",is(1)));

        verify(manageUserProfileService, times(1)).getUserDataById(Mockito.anyLong());
        verifyNoMoreInteractions(manageUserProfileService);
    }

    @Test
    public void deleteUser() throws Exception{
        mockMvc.perform(delete("/user/delete")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userRepo.get(0))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",is(true)));

        verify(manageUserProfileService, times(1)).removeUser(Mockito.any(UserDto.class));
        verifyNoMoreInteractions(manageUserProfileService);
    }

    @Test
    public void deleteUserNotFoundException() throws Exception {
        given(manageUserProfileService.removeUser(Mockito.any(UserDto.class))).willThrow(UserNotFoundException.class);

        mockMvc.perform(delete("/user/delete")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userRepo.get(0))))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetUserNotFoundException() throws Exception{
        given(manageUserProfileService.getUserDataById(Mockito.anyLong())).willThrow(UserNotFoundException.class);

        mockMvc.perform(get("/user/get").param("userId", "1547547"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetUserNotNumericalIdException() throws Exception{
        mockMvc.perform(get("/user/get").param("userId", "hg"))
                .andExpect(status().isBadRequest());
              //TODO check if contains message  .andExpect(status().reason(contains("passed")));
    }


    private List<UserDto> resetUserRepo(){
        List<UserDto> userRepo = new LinkedList<>();
        for(int i=1; i<6;i++) {
            UserDto userDto = new UserDto();
            userDto.setId(Long.valueOf(i));
            userRepo.add(userDto);
        }
        return userRepo;
    }

    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}