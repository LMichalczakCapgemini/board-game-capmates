package com.capgemini.jstk.boardgamecapmates.service.validator;

import com.capgemini.jstk.boardgamecapmates.mapper.dto.BoardGameDto;

public interface IBoardGameValidator {
    void validate(BoardGameDto boardGameDto);
}
