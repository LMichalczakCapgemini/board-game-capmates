package com.capgemini.jstk.boardgamecapmates.service;

import com.capgemini.jstk.boardgamecapmates.entity.BoardGame;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.BoardGameDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;

import java.util.LinkedList;

public interface IManageUserGamesCollectionService {

    LinkedList<BoardGame> getGamesCollection(UserDto userDto);
    void removeGameFromCollection(UserDto userDto, BoardGameDto boardGameDto);
    void addGameToCollection(UserDto userDto, BoardGameDto boardGameDto);
}
