package com.capgemini.jstk.boardgamecapmates.service.validator.impl;

import com.capgemini.jstk.boardgamecapmates.commons.ToShortPasswordException;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import com.capgemini.jstk.boardgamecapmates.service.validator.IUserInfoValidator;
import org.springframework.beans.factory.annotation.Value;

public class PasswordValidator implements IUserInfoValidator {

    private Integer requiredCharacters = 8;

    @Override
    public void validate(UserDto userDto) {
        if(userDto.getPassword().length() < requiredCharacters){
            throw new ToShortPasswordException();
        }
    }
}
