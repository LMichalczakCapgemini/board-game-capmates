package com.capgemini.jstk.boardgamecapmates.mapper;

import com.capgemini.jstk.boardgamecapmates.entity.User;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UserMapper {

    public List<Optional<UserDto>> map(List<Optional<User>> userList){
        List<Optional<UserDto>> optionalUserDtoList = new LinkedList<>();
        if(!userList.isEmpty()){
            optionalUserDtoList = userList.stream()
                    .map(oC -> map(oC)).collect(Collectors.toList());
        }
        return optionalUserDtoList;
    }

    public Optional<UserDto> map(Optional<User> user){
        UserDto userDto = new UserDto();
        if(user.isPresent()){
            userDto.setId(user.get().getId());
            userDto.setFistName(user.get().getFistName());
            userDto.setLastName(user.get().getLastName());
            userDto.setEmail(user.get().getEmail());
            userDto.setPassword(user.get().getPassword());
            userDto.setLevel(user.get().getLevel());
            userDto.setPoints(user.get().getPoints());
            userDto.setBoardGameList(user.get().getBoardGameList());
            userDto.setLifeMotto(user.get().getLifeMotto());
            userDto.setAvailabilityDayList(user.get().getAvailabilityDayList());
        }
        return Optional.ofNullable(userDto);
    }

    public User map(UserDto userDto){
        User user = new User();
        setUserIdIfPresent(userDto, user);
        user.setFistName(userDto.getFistName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setLevel(userDto.getLevel());
        user.setPoints(userDto.getPoints());
        user.setBoardGameList(userDto.getBoardGameList());
        user.setLifeMotto(userDto.getLifeMotto());
        user.setAvailabilityDayList(userDto.getAvailabilityDayList());
        return user;
    }

    private void setUserIdIfPresent(UserDto userDto, User user){
        if(userDto.getId() != null){
            user.setId(userDto.getId());
        }
    }
}
