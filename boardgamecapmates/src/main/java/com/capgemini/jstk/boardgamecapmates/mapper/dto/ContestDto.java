package com.capgemini.jstk.boardgamecapmates.mapper.dto;

import java.util.Objects;

public class ContestDto {

    private Long id;
    private Long playerOne;
    private Long playerTwo;
    private Long winner;
    private Long boardGame;

    public Long getPlayerOne() {
        return playerOne;
    }

    public void setPlayerOne(Long playerOne) {
        this.playerOne = playerOne;
    }

    public Long getPlayerTwo() {
        return playerTwo;
    }

    public void setPlayerTwo(Long playerTwo) {
        this.playerTwo = playerTwo;
    }

    public Long getWinner() {
        return winner;
    }

    public void setWinner(Long winner) {
        this.winner = winner;
    }

    public Long getBoardGame() {
        return boardGame;
    }

    public void setBoardGame(Long boardGame) {
        this.boardGame = boardGame;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContestDto)) return false;
        ContestDto that = (ContestDto) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getPlayerOne(), that.getPlayerOne()) &&
                Objects.equals(getPlayerTwo(), that.getPlayerTwo()) &&
                Objects.equals(getWinner(), that.getWinner()) &&
                Objects.equals(getBoardGame(), that.getBoardGame());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getPlayerOne(), getPlayerTwo(), getWinner(), getBoardGame());
    }
}
