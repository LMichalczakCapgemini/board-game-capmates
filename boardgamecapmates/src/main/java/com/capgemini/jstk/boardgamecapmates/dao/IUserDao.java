package com.capgemini.jstk.boardgamecapmates.dao;

import com.capgemini.jstk.boardgamecapmates.entity.AvailabilityDay;
import com.capgemini.jstk.boardgamecapmates.entity.User;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;

import java.util.List;
import java.util.Optional;

public interface IUserDao {
    Optional<User> findById(Long id);
    boolean save(UserDto userDto);
    List<Optional<User>> findUsersWithSameAvailability(List<Long> availabilityDayIds);
    boolean delete(UserDto userDto);
}
