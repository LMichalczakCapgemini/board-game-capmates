package com.capgemini.jstk.boardgamecapmates.entity;

public class Contest {
    private static Long NUM = 0L;

    private Long id;
    private Long playerOne;
    private Long playerTwo;
    private Long winner;
    private Long boardGame;

    public Contest(){}

    public Contest(Long playerOne, Long playerTwo, Long winner, Long boardGame){
        this.id = NUM++;
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.winner = winner;
        this.boardGame = boardGame;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlayerOne() {
        return playerOne;
    }

    public void setPlayerOne(Long playerOne) {
        this.playerOne = playerOne;
    }

    public Long getPlayerTwo() {
        return playerTwo;
    }

    public void setPlayerTwo(Long playerTwo) {
        this.playerTwo = playerTwo;
    }

    public Long getWinner() {
        return winner;
    }

    public void setWinner(Long winner) {
        this.winner = winner;
    }

    public Long getBoardGame() {
        return boardGame;
    }

    public void setBoardGame(Long boardGame) {
        this.boardGame = boardGame;
    }

    public boolean userParticipated(Long userId){
        return playerOne.equals(userId) ||
                playerTwo.equals(userId);
    }

    //TODO zastanowic sie czy trzymac date rozgrywki (dzien)
}
