package com.capgemini.jstk.boardgamecapmates.restService;

import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.SearchContestCriteria;
import com.capgemini.jstk.boardgamecapmates.service.impl.SearchContentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/contests")
public class SearchContestsController {

    private SearchContentsService searchContentsService;

    @Autowired
    public void setSearchContentsService(SearchContentsService searchContentsService) {
        this.searchContentsService = searchContentsService;
    }

    @PostMapping("/search")
    public List<ContestDto> searchContests(@RequestBody SearchContestCriteria searchContestCriteria){

        return searchContentsService.filterSearchContests(searchContestCriteria);
    }
}