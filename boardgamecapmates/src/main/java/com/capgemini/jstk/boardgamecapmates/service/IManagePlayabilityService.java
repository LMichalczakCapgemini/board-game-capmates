package com.capgemini.jstk.boardgamecapmates.service;

import com.capgemini.jstk.boardgamecapmates.mapper.dto.AvailabilityDayDto;
import com.capgemini.jstk.boardgamecapmates.mapper .dto.UserDto;

import java.util.List;

public interface IManagePlayabilityService {
    void addAvailabilityHours(UserDto userDto, AvailabilityDayDto availabilityDayDto);
    void deletePeriodWithComment(UserDto userDto, AvailabilityDayDto availabilityDayDto, String comment);
    List<UserDto> getUsersWithSimilarPlayability(UserDto userDto);
    void createContestUsersWithSimilarPlayability(UserDto userDto);

}
