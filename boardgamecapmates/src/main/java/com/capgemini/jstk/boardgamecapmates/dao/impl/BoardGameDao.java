package com.capgemini.jstk.boardgamecapmates.dao.impl;

import com.capgemini.jstk.boardgamecapmates.dao.IBoardGameDao;
import com.capgemini.jstk.boardgamecapmates.entity.BoardGame;
import com.capgemini.jstk.boardgamecapmates.mapper.BoardGameMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.BoardGameDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class BoardGameDao implements IBoardGameDao {
    private static List<BoardGame> boardGames;

    private BoardGameMapper mapper;

    @Autowired
    public BoardGameDao(BoardGameMapper mapper) {
        this.mapper = mapper;
    }

    static {
        resetBoardGames();
    }

    @Override
    public Optional<BoardGame> findById(Long id) {
        return boardGames.stream().filter( bG -> bG.getId().equals(id)).findFirst();
    }

    @Override
    public boolean save(BoardGameDto boardGameDto) {
        BoardGame boardGame = mapper.map(boardGameDto);
        Optional<BoardGame> foundBoardGame = findById(boardGame.getId());
        if(foundBoardGame.isPresent()){
            boardGames.remove(foundBoardGame.get());
        }
        return boardGames.add(boardGame);
    }

    private static void resetBoardGames(){
        boardGames = new ArrayList<>();
        boardGames.add(new BoardGame("Chess", "Standard Chess game"));
        boardGames.add(new BoardGame("Monopoly", "Monopoly is a board game where players roll two six-sided dice to move around the game board, buying and trading properties, and develop them with houses and hotels."));
        boardGames.add(new BoardGame("Dixit", "One player is the storyteller for the turn and looks at the images on the 6 cards in her hand."));
        boardGames.add(new BoardGame("Poker", "Poker texas holdem"));
    }


}
