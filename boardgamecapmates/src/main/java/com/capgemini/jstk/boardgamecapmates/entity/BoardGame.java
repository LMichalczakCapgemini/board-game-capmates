package com.capgemini.jstk.boardgamecapmates.entity;

public class BoardGame {
    private static Long NUM = 0L;

    private Long id;
    private String name;
    private String description;

    public BoardGame(){}

    public BoardGame(String name, String description){
        this.id = NUM++;
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
