package com.capgemini.jstk.boardgamecapmates.dao.impl;

import com.capgemini.jstk.boardgamecapmates.dao.IUserDao;
import com.capgemini.jstk.boardgamecapmates.entity.AvailabilityDay;
import com.capgemini.jstk.boardgamecapmates.entity.User;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import com.capgemini.jstk.boardgamecapmates.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserDao implements IUserDao {

    private UserMapper mapper;

    private static List<User> users;

    static {
        resetUsers();
    }

    @Autowired
    public UserDao(UserMapper mapper) {
        this.mapper = mapper;
    }



    @Override
    public Optional<User> findById(Long id) {
        return users.stream().filter(u -> u.getId().equals(id)).findFirst();
    }

    public List<Optional<User>> findBetterUsers(Long id){
        return users.stream()
                .filter(u -> u.getPoints() > findById(id).get().getPoints())
                .map(u-> Optional.of(u))
                .collect(Collectors.toList());
    }
    
    public List<Optional<User>> findUsersWithSameAvailability(List<Long> availabilityDayIds){
        return users.stream()
                .filter(u -> u.hasSameAvailability(availabilityDayIds))
                .map(u-> Optional.of(u))
                .collect(Collectors.toList());
    }

    @Override
    public boolean delete(UserDto userDto) {
        User user = mapper.map(userDto);
        Optional<User> foundUser = findById(user.getId());
        if(foundUser.isPresent()){
            users.remove(foundUser.get());
            return true;
        }
        return false;
    }

    @Override
    public boolean save(UserDto userDto) {
        User user = mapper.map(userDto);
        Optional<User> foundUser = findById(user.getId());
        if(foundUser.isPresent()){
            users.remove(foundUser.get());
        }
        return users.add(user);
    }


    private static void resetUsers(){
        users = new ArrayList<>();

        LinkedList<Long> boardGamesIndexes = new LinkedList<Long>();
        boardGamesIndexes.add(1L);
        boardGamesIndexes.add(3L);

        LinkedList<Long> availabilityIndexes = new LinkedList<Long>();
        availabilityIndexes.add(1L);
        availabilityIndexes.add(3L);


        users.add(new User.Builder("fn1", "ln1", "e@1.pl", "p1").points(1L).build());
        users.add(new User.Builder("fn2", "ln2", "e@2.pl", "p2")
                .boardGameSet(boardGamesIndexes).points(2L).build());
        users.add(new User.Builder("fn3", "ln2", "e@3.pl", "p3").points(4L).build());
        users.add(new User.Builder("fn4", "ln3", "e@4.pl", "p4").avabilityDaySet(availabilityIndexes).build());
        users.add(new User.Builder("fn5", "ln4", "e@5.pl", "p5").avabilityDaySet(availabilityIndexes).build());
    }
}
