package com.capgemini.jstk.boardgamecapmates.service;

import com.capgemini.jstk.boardgamecapmates.enums.UserInformationType;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;

public interface IManageUserProfileService {
    String getUserInformation(UserDto userDto, UserInformationType userInformationType);

    void setUserInformation(UserDto userDto, UserInformationType userInformationType, String information);

    UserDto getUserDataById(Long userId);

    boolean removeUser(UserDto userDto);
}
