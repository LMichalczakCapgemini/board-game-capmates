package com.capgemini.jstk.boardgamecapmates.mapper;

import com.capgemini.jstk.boardgamecapmates.entity.BoardGame;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.BoardGameDto;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class BoardGameMapper {

    public Optional<BoardGameDto> map(Optional<BoardGame> boardGame){
        BoardGameDto boardGameDto = new BoardGameDto();
        if(boardGame.isPresent()){
            boardGameDto.setId(boardGame.get().getId());
            boardGameDto.setDescription(boardGame.get().getDescription());
            boardGameDto.setName(boardGame.get().getName());
        }
        return Optional.ofNullable(boardGameDto);
    }

    public BoardGame map(BoardGameDto boardGameDto){
        BoardGame boardGame = new BoardGame();
        setBoardGameIdIfPresent(boardGameDto, boardGame);
        boardGame.setDescription(boardGameDto.getDescription());
        boardGame.setName(boardGameDto.getName());
        return boardGame;
    }

    private void setBoardGameIdIfPresent(BoardGameDto boardGameDto, BoardGame boardGame){
        if(boardGameDto.getId() != null){
            boardGame.setId(boardGameDto.getId());
        }
    }
}
