package com.capgemini.jstk.boardgamecapmates.service.impl;

import com.capgemini.jstk.boardgamecapmates.commons.NotFoundBoardGameInUserListException;
import com.capgemini.jstk.boardgamecapmates.commons.UserHasAlreadyThatGame;
import com.capgemini.jstk.boardgamecapmates.dao.IBoardGameDao;
import com.capgemini.jstk.boardgamecapmates.dao.IUserDao;
import com.capgemini.jstk.boardgamecapmates.entity.BoardGame;
import com.capgemini.jstk.boardgamecapmates.mapper.BoardGameMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.UserMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.BoardGameDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import com.capgemini.jstk.boardgamecapmates.service.IManageUserGamesCollectionService;
import com.capgemini.jstk.boardgamecapmates.service.validator.impl.BoardGameValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.Optional;

@Service
public class ManageUserGamesCollectionService implements IManageUserGamesCollectionService {

    private IUserDao userDao;
    private IBoardGameDao boardGameDao;

    @Autowired
    public ManageUserGamesCollectionService(IUserDao userDao, IBoardGameDao boardGameDao){
        this.userDao = userDao;
        this.boardGameDao = boardGameDao;
    }

    @Override
    public LinkedList<BoardGame> getGamesCollection(UserDto userDto) {
        LinkedList<BoardGame> userBoardGames = new LinkedList<>();

        userDto.getBoardGameList().forEach(gameIndex -> {
            Optional<BoardGame> boardGame = boardGameDao.findById(gameIndex);
            if(boardGame.isPresent()){
                userBoardGames.add(boardGame.get());
            }
        });

        return userBoardGames;
    }

    @Override
    public void removeGameFromCollection(UserDto userDto, BoardGameDto boardGameDto) {
        boolean contains = userDto.getBoardGameList().remove(boardGameDto.getId());

        if(!contains){
            throw new NotFoundBoardGameInUserListException();
        }

        userDao.save(userDto);
    }

    @Override
    public void addGameToCollection(UserDto userDto, BoardGameDto boardGameDto) {
        BoardGameValidator boardGameValidator = new BoardGameValidator();
        boardGameValidator.validate(boardGameDto);
        boardGameDao.save(boardGameDto);

        if(userDto.getBoardGameList().contains(boardGameDto.getId())){
            throw new UserHasAlreadyThatGame();
        }

        userDto.getBoardGameList().add(boardGameDto.getId());
        userDao.save(userDto);

    }



}
