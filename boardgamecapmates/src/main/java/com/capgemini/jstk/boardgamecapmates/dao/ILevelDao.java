package com.capgemini.jstk.boardgamecapmates.dao;

import com.capgemini.jstk.boardgamecapmates.entity.Level;

import java.util.Optional;

public interface ILevelDao {
    Optional<Level> findById(Long id);
}
