package com.capgemini.jstk.boardgamecapmates.mapper.dto;

import com.capgemini.jstk.boardgamecapmates.enums.LevelType;

public class UserStatisticsDto {
    private LevelType userLevel;
    Long winGames;
    Long loseGames;
    Long userPosition;
    ContestDto userContests;

    public LevelType getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(LevelType userLevel) {
        this.userLevel = userLevel;
    }

    public Long getWinGames() {
        return winGames;
    }

    public void setWinGames(Long winGames) {
        this.winGames = winGames;
    }

    public Long getLoseGames() {
        return loseGames;
    }

    public void setLoseGames(Long loseGames) {
        this.loseGames = loseGames;
    }

    public Long getUserPosition() {
        return userPosition;
    }

    public void setUserPosition(Long userPosition) {
        this.userPosition = userPosition;
    }

    public ContestDto getUserContests() {
        return userContests;
    }

    public void setUserContests(ContestDto userContests) {
        this.userContests = userContests;
    }
}
