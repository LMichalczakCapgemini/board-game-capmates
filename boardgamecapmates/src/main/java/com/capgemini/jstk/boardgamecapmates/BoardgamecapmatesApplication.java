package com.capgemini.jstk.boardgamecapmates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoardgamecapmatesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoardgamecapmatesApplication.class, args);
	}
}
