package com.capgemini.jstk.boardgamecapmates.dao.impl;

import com.capgemini.jstk.boardgamecapmates.dao.IAvailabilityDayDao;
import com.capgemini.jstk.boardgamecapmates.entity.AvailabilityDay;
import com.capgemini.jstk.boardgamecapmates.entity.User;
import com.capgemini.jstk.boardgamecapmates.mapper.AvailabilityMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.AvailabilityDayDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class AvailabilityDayDao implements IAvailabilityDayDao {

    private static List<AvailabilityDay> availabilityDays;

    private AvailabilityMapper mapper;

    @Autowired
    public AvailabilityDayDao(AvailabilityMapper mapper) {
        this.mapper = mapper;
    }

    static {
        resetAvailabilityDays();
    }



    @Override
    public Optional<AvailabilityDay> findById(Long id) {
        return availabilityDays.stream().filter(aD -> aD.getId().equals(id)).findFirst();
    }

    @Override
    public List<Optional<AvailabilityDay>> findByUserId(Long userId){
        return availabilityDays.stream().filter(aD-> aD.getUserId().equals(userId))
                .map(ad -> Optional.of(ad))
                .collect(Collectors.toList());
    }

    @Override
    public boolean save(AvailabilityDayDto availabilityDayDto) {
        AvailabilityDay availabilityDay = mapper.map(availabilityDayDto);
        Optional<AvailabilityDay> foundAvailabilityDay = findById(availabilityDay.getId());
        if(foundAvailabilityDay.isPresent()){
            availabilityDays.remove(foundAvailabilityDay.get());
        }
        return availabilityDays.add(availabilityDay);
    }

    private static void resetAvailabilityDays(){
        availabilityDays = new ArrayList<>();
        availabilityDays.add(new AvailabilityDay(1L, DayOfWeek.MONDAY));
        availabilityDays.add(new AvailabilityDay(2L, DayOfWeek.TUESDAY));
        availabilityDays.add(new AvailabilityDay(2L, DayOfWeek.WEDNESDAY));
        availabilityDays.add(new AvailabilityDay(3L, DayOfWeek.FRIDAY));
        availabilityDays.add(new AvailabilityDay(3L, DayOfWeek.SATURDAY));
        availabilityDays.add(new AvailabilityDay(3L, DayOfWeek.WEDNESDAY));
        availabilityDays.add(new AvailabilityDay(4L, DayOfWeek.THURSDAY));
        availabilityDays.add(new AvailabilityDay(4L, DayOfWeek.SATURDAY));
    }

}
