package com.capgemini.jstk.boardgamecapmates.service;

import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.LevelDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;

import java.util.List;

public interface IGetUserStatisticsService {
    Long getUserWon(UserDto userDto);
    Long getUserLost(UserDto userDto);
    LevelDto getUserLevel(UserDto userDto);
    Long getUserRankingPosition(UserDto userDto);
    List<ContestDto> getUserContestHistory(UserDto userDto);
}
