package com.capgemini.jstk.boardgamecapmates.dao;

import com.capgemini.jstk.boardgamecapmates.entity.AvailabilityDay;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.AvailabilityDayDto;

import java.util.List;
import java.util.Optional;

public interface IAvailabilityDayDao {
    Optional<AvailabilityDay> findById(Long id);
    List<Optional<AvailabilityDay>> findByUserId(Long userId);
    boolean save(AvailabilityDayDto availabilityDayDto);
}
