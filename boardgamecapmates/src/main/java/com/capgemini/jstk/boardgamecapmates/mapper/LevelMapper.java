package com.capgemini.jstk.boardgamecapmates.mapper;

import com.capgemini.jstk.boardgamecapmates.entity.Level;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.LevelDto;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class LevelMapper {

    public Optional<LevelDto> map (Optional<Level> level){
        LevelDto levelDto = new LevelDto();
        if(level.isPresent()){
            levelDto.setId(level.get().getId());
            levelDto.setLevelType(level.get().getLevelType());
        }
        return Optional.ofNullable(levelDto);
    }

    public Level map(LevelDto levelDto){
        Level level = new Level();
        setLevelIdIfPresent(levelDto, level);
        level.setLevelType(levelDto.getLevelType());
        return level;
    }

    private void setLevelIdIfPresent(LevelDto levelDto, Level level){
        if(levelDto.getId() != null){
            level.setId(levelDto.getId());
        }
    }
}
