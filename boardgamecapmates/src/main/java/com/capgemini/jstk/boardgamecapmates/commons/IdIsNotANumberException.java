package com.capgemini.jstk.boardgamecapmates.commons;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IdIsNotANumberException extends RuntimeException {
    public IdIsNotANumberException(String id) {
        super("passed user id: '" + id + "' is not a number.");
    }
}
