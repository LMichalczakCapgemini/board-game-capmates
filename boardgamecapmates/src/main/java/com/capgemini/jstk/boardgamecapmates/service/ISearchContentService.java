package com.capgemini.jstk.boardgamecapmates.service;

import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.SearchContestCriteria;

import java.util.List;

public interface ISearchContentService {
    List<ContestDto> filterSearchContests(SearchContestCriteria searchContestCriteria);
}
