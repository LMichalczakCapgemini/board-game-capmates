package com.capgemini.jstk.boardgamecapmates.mapper.dto;

public class SearchContestCriteria {
    private UserDto memberUserDto;
    private BoardGameDto boardGameDto;
    private UserDto winnerUserDto;

    public SearchContestCriteria() {
    }

    public SearchContestCriteria(UserDto memberUserDto, BoardGameDto boardGameDto, UserDto winnerUserDto) {
        this.memberUserDto = memberUserDto;
        this.boardGameDto = boardGameDto;
        this.winnerUserDto = winnerUserDto;
    }

    public UserDto getMemberUserDto() {
        return memberUserDto;
    }

    public void setMemberUserDto(UserDto memberUserDto) {
        this.memberUserDto = memberUserDto;
    }

    public BoardGameDto getBoardGameDto() {
        return boardGameDto;
    }

    public void setBoardGameDto(BoardGameDto boardGameDto) {
        this.boardGameDto = boardGameDto;
    }

    public UserDto getWinnerUserDto() {
        return winnerUserDto;
    }

    public void setWinnerUserDto(UserDto winnerUserDto) {
        this.winnerUserDto = winnerUserDto;
    }
}
