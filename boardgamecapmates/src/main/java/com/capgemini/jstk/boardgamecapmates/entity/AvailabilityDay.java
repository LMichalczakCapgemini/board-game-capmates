package com.capgemini.jstk.boardgamecapmates.entity;

import java.time.DayOfWeek;

public class AvailabilityDay {
    private static Long NUM = 0L;

    private Long id;
    private Long userId;
    private boolean status;
    private String comment;
    private DayOfWeek dayOfWeek;

    public AvailabilityDay(){}

    public AvailabilityDay(Long userId, DayOfWeek dayOfWeek){
        this.id = NUM++;
        this.userId = userId;
        this.dayOfWeek = dayOfWeek;
        this.status = true;
        this.comment = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
}
