package com.capgemini.jstk.boardgamecapmates.mapper.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.LinkedList;

public class UserDto {
    private Long id;
    private String fistName;
    private String lastName;
    private String email;
    @JsonIgnore
    private String password;

    private Long level;
    private Long points;
    private LinkedList<Long> boardGameList;
    private String lifeMotto;
    private LinkedList<Long> availabilityDayList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }

    public LinkedList<Long> getBoardGameList() {
        return boardGameList;
    }

    public void setBoardGameList(LinkedList<Long> boardGameList) {
        this.boardGameList = boardGameList;
    }

    public String getLifeMotto() {
        return lifeMotto;
    }

    public void setLifeMotto(String lifeMotto) {
        this.lifeMotto = lifeMotto;
    }

    public LinkedList<Long> getAvailabilityDayList() {
        return availabilityDayList;
    }

    public void setAvailabilityDayList(LinkedList<Long> availabilityDayList) {
        this.availabilityDayList = availabilityDayList;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", fistName='" + fistName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", level=" + level +
                ", points=" + points +
                ", boardGameList=" + boardGameList +
                ", lifeMotto='" + lifeMotto + '\'' +
                ", availabilityDayList=" + availabilityDayList +
                '}';
    }
}
