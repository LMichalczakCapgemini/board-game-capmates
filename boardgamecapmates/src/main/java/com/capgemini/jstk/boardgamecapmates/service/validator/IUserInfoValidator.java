package com.capgemini.jstk.boardgamecapmates.service.validator;

import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import org.springframework.stereotype.Component;

@Component
public interface IUserInfoValidator {
    void validate(UserDto userDto);
}
