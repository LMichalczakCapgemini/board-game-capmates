package com.capgemini.jstk.boardgamecapmates.service.validator.impl;

import com.capgemini.jstk.boardgamecapmates.commons.NotValidEmailAddressException;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import com.capgemini.jstk.boardgamecapmates.service.validator.IUserInfoValidator;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class EmailValidator implements IUserInfoValidator {
    @Override
    public void validate(UserDto userDto) {
        try {
            InternetAddress emailAddress = new InternetAddress(userDto.getEmail());
            emailAddress.validate();
        } catch (AddressException ex) {
            throw new NotValidEmailAddressException();
        }
    }
}
