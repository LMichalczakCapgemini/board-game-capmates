package com.capgemini.jstk.boardgamecapmates.service.impl;

import com.capgemini.jstk.boardgamecapmates.commons.NoUsersWithSameAvailabilityHoursException;
import com.capgemini.jstk.boardgamecapmates.commons.UserHasAlreadyThatAvailabilityDayException;
import com.capgemini.jstk.boardgamecapmates.dao.impl.AvailabilityDayDao;
import com.capgemini.jstk.boardgamecapmates.dao.impl.ContestDao;
import com.capgemini.jstk.boardgamecapmates.dao.impl.UserDao;
import com.capgemini.jstk.boardgamecapmates.entity.AvailabilityDay;
import com.capgemini.jstk.boardgamecapmates.mapper.AvailabilityMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.UserMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.AvailabilityDayDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import com.capgemini.jstk.boardgamecapmates.service.IManagePlayabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class ManagePlayabilityService implements IManagePlayabilityService {


    private UserDao userDao;

    private UserMapper userMapper;

    private AvailabilityDayDao availabilityDayDao;

    private ContestDao contestDao;

    @Autowired
    public ManagePlayabilityService(UserDao userDao, UserMapper userMapper,
                                    AvailabilityDayDao availabilityDayDao, ContestDao contestDao) {
        this.userDao = userDao;
        this.userMapper = userMapper;
        this.availabilityDayDao = availabilityDayDao;
        this.contestDao = contestDao;
    }

    @Override
    public void addAvailabilityHours(UserDto userDto, AvailabilityDayDto availabilityDayDto) {
        Long userId = userDto.getId();
        List<Optional<AvailabilityDay>> optAvailabilityDayList =  availabilityDayDao.findByUserId(userId);

        optAvailabilityDayList.stream()
                .forEach(oA -> {
            if(oA.isPresent()) {
                if (oA.get().getDayOfWeek().equals(availabilityDayDto.getDayOfWeek())) {
                    throw new UserHasAlreadyThatAvailabilityDayException();
                }
            }
        });

        availabilityDayDao.save(availabilityDayDto); //TODO validate

        Long availabilityId = availabilityDayDto.getId();
        userDto.getAvailabilityDayList().add(availabilityId);
        userDao.save(userDto); //TODO validate
    }

    @Override
    public void deletePeriodWithComment(UserDto userDto, AvailabilityDayDto availabilityDayDto, String comment) {
        userDto.getAvailabilityDayList().remove(availabilityDayDto.getId());

        availabilityDayDto.setComment(comment);
        availabilityDayDto.setStatus(false);
        availabilityDayDao.save(availabilityDayDto);
    }

    @Override
    public List<UserDto> getUsersWithSimilarPlayability(UserDto userDto) {
        List<UserDto> userDtoList = new LinkedList<>();

        List<Optional<UserDto>> optionalUserDtoList =  userMapper.map(userDao.findUsersWithSameAvailability(userDto.getAvailabilityDayList()));
        optionalUserDtoList.stream()
                .forEach(oU -> {
                    if(oU.isPresent() && !oU.get().getId().equals(userDto.getId())){
                        userDtoList.add(oU.get());
                    }
                });

        return userDtoList;
    }

    @Override
    public void createContestUsersWithSimilarPlayability(UserDto userDto) {
        List<UserDto> sameAvailabilityUsers = getUsersWithSimilarPlayability(userDto);
        if(sameAvailabilityUsers.isEmpty()){
            throw new NoUsersWithSameAvailabilityHoursException();
        }
        ContestDto contestDto = new ContestDto();
        contestDto.setPlayerOne(userDto.getId());
        contestDto.setPlayerTwo(sameAvailabilityUsers.get(0).getId());
        contestDao.save(contestDto);
    }
}
