package com.capgemini.jstk.boardgamecapmates.mapper.dto;

import com.capgemini.jstk.boardgamecapmates.enums.LevelType;

public class LevelDto {
    private Long id;
    private LevelType levelType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LevelType getLevelType() {
        return levelType;
    }

    public void setLevelType(LevelType levelType) {
        this.levelType = levelType;
    }
}
