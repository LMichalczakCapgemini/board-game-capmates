package com.capgemini.jstk.boardgamecapmates.mapper;

import com.capgemini.jstk.boardgamecapmates.entity.Contest;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ContestMapper {



    public List<Optional<ContestDto>> map(List<Optional<Contest>> contestList){
        List<Optional<ContestDto>> optionalContestDtoList = new LinkedList<>();
        if(!contestList.isEmpty()){
            optionalContestDtoList = contestList.stream()
                    .map(oC -> map(oC)).collect(Collectors.toList());
        }
        return optionalContestDtoList;
    }

    public Optional<ContestDto> map(Optional<Contest> contest){
        ContestDto contestDto = new ContestDto();
        if(contest.isPresent()){
            contestDto.setPlayerOne(contest.get().getPlayerOne());
            contestDto.setPlayerTwo(contest.get().getPlayerTwo());
            contestDto.setWinner(contest.get().getWinner());
            contestDto.setBoardGame(contest.get().getBoardGame());
        }
        return Optional.ofNullable(contestDto);
    }

    public Contest map(ContestDto contestDto){
        Contest contest = new Contest();
        setContestIdIfPresent(contestDto, contest);
        contest.setPlayerOne(contestDto.getPlayerOne());
        contest.setPlayerTwo(contestDto.getPlayerTwo());
        contest.setWinner(contestDto.getWinner());
        contest.setBoardGame(contestDto.getBoardGame());
        return contest;
    }

    private void setContestIdIfPresent(ContestDto contestDto, Contest contest){
        if(contestDto.getId() != null){
            contest.setId(contestDto.getId());
        }
    }
}
