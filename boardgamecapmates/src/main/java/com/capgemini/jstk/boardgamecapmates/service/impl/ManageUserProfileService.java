package com.capgemini.jstk.boardgamecapmates.service.impl;

import com.capgemini.jstk.boardgamecapmates.commons.UserNotFoundException;
import com.capgemini.jstk.boardgamecapmates.dao.IUserDao;
import com.capgemini.jstk.boardgamecapmates.dao.impl.UserDao;
import com.capgemini.jstk.boardgamecapmates.entity.User;
import com.capgemini.jstk.boardgamecapmates.enums.UserInformationType;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import com.capgemini.jstk.boardgamecapmates.mapper.UserMapper;
import com.capgemini.jstk.boardgamecapmates.service.IManageUserProfileService;
import com.capgemini.jstk.boardgamecapmates.service.validator.IUserInfoValidator;
import com.capgemini.jstk.boardgamecapmates.service.validator.UserInfoValidatorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ManageUserProfileService implements IManageUserProfileService {

    private IUserDao userDao;
    private UserMapper userMapper;
    private UserInfoValidatorFactory userInfoValidatorFactory;

    @Autowired
    public ManageUserProfileService(UserDao userDao, UserMapper userMapper){
        this.userDao = userDao;
        this.userMapper = userMapper;
        this.userInfoValidatorFactory = new UserInfoValidatorFactory();
    }

    @Override
    public String getUserInformation(UserDto userDto, UserInformationType userInformationType) {
        Long userId = userDto.getId();
        Optional<User> foundUser = userDao.findById(userId);
        if(!foundUser.isPresent()){
            throw new UserNotFoundException(userId);
        }
        Optional<UserDto> foundUserDto = userMapper.map(foundUser);
        return userInformationType.getInformation(foundUserDto.get());
    }

    @Override
    public void setUserInformation(UserDto userDto, UserInformationType userInformationType, String information) {
        userInformationType.setInformation(userDto, information);
        IUserInfoValidator validator = userInfoValidatorFactory.produceValidator(userInformationType);
        validator.validate(userDto);
        userDao.save(userDto);
    }

    @Override
    public UserDto getUserDataById(Long userId){
        Optional<User> optionalUserDto = userDao.findById(userId);
        if(!optionalUserDto.isPresent()){
            throw new UserNotFoundException(userId);
        }
        Optional<UserDto> userDto = userMapper.map(optionalUserDto);
        return userDto.get();
    }

    @Override
    public boolean removeUser(UserDto userDto) {
        boolean result = userDao.delete(userDto);
        if(!result){
            throw new UserNotFoundException(userDto.getId());
        }
        return true;
    }


}
