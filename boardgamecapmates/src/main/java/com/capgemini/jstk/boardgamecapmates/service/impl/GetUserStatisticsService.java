package com.capgemini.jstk.boardgamecapmates.service.impl;

import com.capgemini.jstk.boardgamecapmates.dao.impl.ContestDao;
import com.capgemini.jstk.boardgamecapmates.dao.impl.LevelDao;
import com.capgemini.jstk.boardgamecapmates.dao.impl.UserDao;
import com.capgemini.jstk.boardgamecapmates.mapper.ContestMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.LevelMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.UserMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.LevelDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import com.capgemini.jstk.boardgamecapmates.service.IGetUserStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class GetUserStatisticsService implements IGetUserStatisticsService {

    private ContestDao contestDao;
    private ContestMapper contestMapper;
    private LevelDao levelDao;
    private LevelMapper levelMapper;
    private UserDao userDao;
    private UserMapper userMapper;

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageUserProfileService.class);


    @Autowired
    public GetUserStatisticsService(ContestDao contestDao, ContestMapper contestMapper, LevelDao levelDao,
                                    LevelMapper levelMapper, UserDao userDao, UserMapper userMapper) {
        this.contestDao = contestDao;
        this.contestMapper = contestMapper;
        this.levelDao = levelDao;
        this.levelMapper = levelMapper;
        this.userDao = userDao;
        this.userMapper = userMapper;
    }

    @Override
    public Long getUserWon(UserDto userDto) {
        logInfoValuesAndTime(userDto);

        Long userId = userDto.getId();
        List<Optional<ContestDto>> optionalUserContestDtoList = contestMapper.map(contestDao.findByUserId(userId));

        return optionalUserContestDtoList.stream()
                .filter(oC -> oC.isPresent() && oC.get().getWinner().equals(userId))
                .count();
    }

    @Override
    public Long getUserLost(UserDto userDto) {
        logInfoValuesAndTime(userDto);

        Long userId = userDto.getId();
        List<Optional<ContestDto>> optionalUserContestDtoList = contestMapper.map(contestDao.findByUserId(userId));

        return optionalUserContestDtoList.stream()
                .filter(oC -> oC.isPresent() && !oC.get().getWinner().equals(userId))
                .count();
    }

    @Override
    public LevelDto getUserLevel(UserDto userDto) {
        logInfoValuesAndTime(userDto);

        Long levelId =  userDto.getLevel();
        return levelMapper.map(levelDao.findById(levelId)).get();
    }

    @Override
    public Long getUserRankingPosition(UserDto userDto) {
        logInfoValuesAndTime(userDto);

        Long userId = userDto.getId();
        List<Optional<UserDto>> optionalUsersList = userMapper.map(userDao.findBetterUsers(userId));

        return 1 + optionalUsersList.stream()
                .filter(ou -> ou.isPresent())
                .count();
    }

    @Override
    public List<ContestDto> getUserContestHistory(UserDto userDto) {
        logInfoValuesAndTime(userDto);

        List<ContestDto> contestDtoList = new LinkedList<>();
        Long userId = userDto.getId();
        List<Optional<ContestDto>> optionalContestDtoList = contestMapper.map(contestDao.findByUserId(userId));

        optionalContestDtoList.stream().forEach(contestDtoOptional ->{
            if(contestDtoOptional.isPresent()){
                contestDtoList.add(contestDtoOptional.get());
            }
        });
        return contestDtoList;
    }

    private void logInfoValuesAndTime(UserDto userDto){
        LOGGER.info(new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()));
        LOGGER.info(userDto.toString());
    }

}
