package com.capgemini.jstk.boardgamecapmates.dao;

import com.capgemini.jstk.boardgamecapmates.entity.BoardGame;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.BoardGameDto;

import java.util.Optional;

public interface IBoardGameDao {
    Optional<BoardGame> findById(Long id);
    boolean save(BoardGameDto boardGameDto);
}
