package com.capgemini.jstk.boardgamecapmates.dao.impl;

import com.capgemini.jstk.boardgamecapmates.dao.IContestDao;
import com.capgemini.jstk.boardgamecapmates.entity.Contest;
import com.capgemini.jstk.boardgamecapmates.mapper.ContestMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.BoardGameDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class ContestDao implements IContestDao {
    private static List<Contest> contests;

    private ContestMapper mapper;

    @Autowired
    public ContestDao(ContestMapper mapper) {
        this.mapper = mapper;
    }

    static {
        resetContests();
    }

    @Override
    public Optional<Contest> findById(Long id) {
        return contests.stream().filter(c -> c.getId().equals(id)).findFirst();
    }

    @Override
    public List<Optional<Contest>> findByUserId(Long userId) {
        return contests.stream()
                .filter(c -> c.userParticipated(userId))
                .map(c-> Optional.of(c))
                .collect(Collectors.toList());
    }

    @Override
    public boolean save(ContestDto contestDto) {
        Contest contest = mapper.map(contestDto);
        Optional<Contest> foundContest = findById(contest.getId());
        if(foundContest.isPresent()){
            contests.remove(foundContest.get());
        }
        return contests.add(contest);
    }

    @Override
    public List<Optional<Contest>> findAllContests() {
        return contests.stream()
                .map(c-> Optional.of(c))
                .collect(Collectors.toList());
    }

    @Override
    public List<Optional<Contest>> findByBoardGameId(Long boardGameId) {
        return contests.stream()
                .filter(c -> c.getBoardGame().equals(boardGameId))
                .map(c-> Optional.of(c))
                .collect(Collectors.toList());
    }


    private static void resetContests(){
        contests = new LinkedList<>();
        contests.add(new Contest(1L, 2L, 1l, 1L));
        contests.add(new Contest(1L, 2L, 1l, 3L));
        contests.add(new Contest(3L, 0L, 3l, 4L));
        contests.add(new Contest(2L, 0L, 0l, 1L));
        contests.add(new Contest(0L, 1L, 1l, 1L));
        contests.add(new Contest(2L, 3L, 2l, 4L));
        contests.add(new Contest(1L, 3L, 1l, 3L));
        contests.add(new Contest(0L, 2L, 0l, 2L));
        contests.add(new Contest(2L, 1L, 1l, 3L));
        contests.add(new Contest(2L, 0L, 2l, 4L));
    }
}
