package com.capgemini.jstk.boardgamecapmates.dao;

import com.capgemini.jstk.boardgamecapmates.entity.Contest;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.BoardGameDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;

import java.util.List;
import java.util.Optional;

public interface IContestDao {
    Optional<Contest> findById(Long id);
    List<Optional<Contest>> findByUserId(Long userId);
    boolean save(ContestDto contestDto);
    List<Optional<Contest>> findAllContests();
    List<Optional<Contest>> findByBoardGameId(Long boardGameId);
}
