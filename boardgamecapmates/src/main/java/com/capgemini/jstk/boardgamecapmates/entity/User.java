package com.capgemini.jstk.boardgamecapmates.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.LinkedList;
import java.util.List;

public class User {
    private static Long NUM = 0L;

    private Long id;
    private String fistName;
    private String lastName;
    private String email;
    @JsonIgnore
    private String password;

    private Long level;
    private Long points;
    private LinkedList<Long> boardGameList;
    private String lifeMotto;
    private LinkedList<Long> availabilityDayList;
    //TODO zamienic listy na sety

    public User(){}

    private User(Builder builder) {
        this.id = builder.id;

        this.fistName = builder.fistName;
        this.lastName = builder.lastName;
        this.email = builder.email;
        this.password = builder.password;

        this.level = builder.level;
        this.points = builder.points;
        this.boardGameList = builder.boardGameList;
        this.lifeMotto = builder.lifeMotto;
        this.availabilityDayList = builder.availabilityDayList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }

    public LinkedList<Long> getBoardGameList() {
        return boardGameList;
    }

    public void setBoardGameList(LinkedList<Long> boardGameList) {
        this.boardGameList = boardGameList;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLifeMotto() {
        return lifeMotto;
    }

    public void setLifeMotto(String lifeMotto) {
        this.lifeMotto = lifeMotto;
    }

    public LinkedList<Long> getAvailabilityDayList() {
        return availabilityDayList;
    }

    public void setAvailabilityDayList(LinkedList<Long> availabilityDayList) {
        this.availabilityDayList = availabilityDayList;
    }

    public boolean hasSameAvailability(List<Long> availabilityDayIds){
        return this.availabilityDayList.stream()
                .anyMatch(aD -> availabilityDayIds.contains(aD));
    }


    public static class Builder{
        private Long id;

        private final String fistName;
        private final String lastName;
        private final String email;
        private final String password;

        private Long level = 0L;
        private Long points = 0L;
        private LinkedList<Long> boardGameList = new LinkedList<>();
        private String lifeMotto = "";
        private LinkedList<Long> availabilityDayList = new LinkedList<>();

        public Builder(String fistName, String lastName, String email, String password){
            this.id = NUM++;

            this.fistName = fistName;
            this.lastName = lastName;
            this.email = email;
            this.password = password;
        }

        public Builder level(Long val){
            level = val;
            return this;
        }

        public Builder points(Long val){
            points = val;
            return this;
        }

        public Builder lifeMotto(String val){
            lifeMotto = val;
            return this;
        }

        public Builder boardGameSet(LinkedList<Long> val){
            boardGameList = val;
            return this;
        }

        public Builder avabilityDaySet(LinkedList<Long> val){
            availabilityDayList = val;
            return this;
        }

        public User build(){
            return new User(this);
        }
    }

}
