package com.capgemini.jstk.boardgamecapmates.service.validator;

import com.capgemini.jstk.boardgamecapmates.enums.UserInformationType;
import com.capgemini.jstk.boardgamecapmates.service.validator.impl.EmailValidator;
import com.capgemini.jstk.boardgamecapmates.service.validator.impl.LifeMottoValidator;
import com.capgemini.jstk.boardgamecapmates.service.validator.impl.NameValidator;
import com.capgemini.jstk.boardgamecapmates.service.validator.impl.PasswordValidator;
import org.springframework.stereotype.Component;

@Component
public class UserInfoValidatorFactory {

    public IUserInfoValidator produceValidator(UserInformationType userInformationType){
        switch (userInformationType){
            case FirstName:
                return new NameValidator();
            case LastName:
                return new NameValidator();
            case Email:
                return new EmailValidator();
            case Password:
                return new PasswordValidator();
            case LifeMotto:
                return new LifeMottoValidator();
        }
        return null;
    }
}
