package com.capgemini.jstk.boardgamecapmates.service.validator.impl;

import com.capgemini.jstk.boardgamecapmates.commons.EmptyNameException;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import com.capgemini.jstk.boardgamecapmates.service.validator.IUserInfoValidator;

public class NameValidator implements IUserInfoValidator {
    @Override
    public void validate(UserDto userDto) {
        if(userDto.getFistName().isEmpty()){
            throw new EmptyNameException();
        }
    }
}
