package com.capgemini.jstk.boardgamecapmates.entity;

import com.capgemini.jstk.boardgamecapmates.enums.LevelType;

public class Level {
    private static Long NUM = 0L;

    private Long id;
    private LevelType levelType;

    public Level(){}

    public Level(LevelType levelType){
        this.id = NUM++;
        this.levelType = levelType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LevelType getLevelType() {
        return levelType;
    }

    public void setLevelType(LevelType levelType) {
        this.levelType = levelType;
    }
}
