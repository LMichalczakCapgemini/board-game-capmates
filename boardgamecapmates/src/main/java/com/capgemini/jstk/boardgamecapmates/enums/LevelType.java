package com.capgemini.jstk.boardgamecapmates.enums;

public enum LevelType {
    Beginner,
    Advanced,
    Master
}
