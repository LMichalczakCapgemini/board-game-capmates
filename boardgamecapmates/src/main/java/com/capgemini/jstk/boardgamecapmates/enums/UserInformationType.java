package com.capgemini.jstk.boardgamecapmates.enums;

import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;

public enum UserInformationType {
    FirstName{
        @Override
        public String getInformation(UserDto userDto) {
            return userDto.getFistName();
        }

        @Override
        public void setInformation(UserDto userDto, String information) {
            userDto.setFistName(information);
        }
    },
    LastName {
        @Override
        public String getInformation(UserDto userDto) {
            return userDto.getLastName();
        }

        @Override
        public void setInformation(UserDto userDto, String information) {
            userDto.setLastName(information);
        }
    },
    Email {
        @Override
        public String getInformation(UserDto userDto) {
            return userDto.getEmail();
        }

        @Override
        public void setInformation(UserDto userDto, String information) {
            userDto.setEmail(information);
        }
    },
    Password {
        @Override
        public String getInformation(UserDto userDto) {
            return userDto.getPassword();
        }

        @Override
        public void setInformation(UserDto userDto, String information) {
            userDto.setPassword(information);
        }
    },
    LifeMotto {
        @Override
        public String getInformation(UserDto userDto) {
            return userDto.getLifeMotto();
        }

        @Override
        public void setInformation(UserDto userDto, String information) {
            userDto.setLifeMotto(information);
        }
    };

    public abstract String getInformation(UserDto userDto);

    public abstract void setInformation(UserDto userDto, String information);
}
