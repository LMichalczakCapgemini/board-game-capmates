package com.capgemini.jstk.boardgamecapmates.service.validator.impl;

import com.capgemini.jstk.boardgamecapmates.commons.ToLongLifeMottoException;
import com.capgemini.jstk.boardgamecapmates.commons.ToShortPasswordException;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import com.capgemini.jstk.boardgamecapmates.service.validator.IUserInfoValidator;

public class LifeMottoValidator implements IUserInfoValidator {

    private Integer maxCharacters = 200;

    @Override
    public void validate(UserDto userDto) {
        if(userDto.getLifeMotto().length() > maxCharacters){
            throw new ToLongLifeMottoException();
        }
    }
}
