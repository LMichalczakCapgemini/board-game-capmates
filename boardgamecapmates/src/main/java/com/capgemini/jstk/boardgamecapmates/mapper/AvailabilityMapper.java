package com.capgemini.jstk.boardgamecapmates.mapper;

import com.capgemini.jstk.boardgamecapmates.entity.AvailabilityDay;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.AvailabilityDayDto;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AvailabilityMapper {

    public Optional<AvailabilityDayDto> map(Optional<AvailabilityDay> availabilityDay){
        AvailabilityDayDto availabilityDayDto = new AvailabilityDayDto();
        if(availabilityDay.isPresent()){
            availabilityDayDto.setId(availabilityDay.get().getId());
            availabilityDayDto.setComment(availabilityDay.get().getComment());
            availabilityDayDto.setDayOfWeek(availabilityDay.get().getDayOfWeek());
            availabilityDayDto.setStatus(availabilityDay.get().getStatus());
            availabilityDayDto.setUserId(availabilityDay.get().getUserId());
        }
        return  Optional.ofNullable(availabilityDayDto);
    }

    public AvailabilityDay map(AvailabilityDayDto availabilityDayDto){
        AvailabilityDay availabilityDay = new AvailabilityDay();
        setAvailabilityDayIdIfPresent(availabilityDayDto, availabilityDay);
        availabilityDay.setComment(availabilityDayDto.getComment());
        availabilityDay.setDayOfWeek(availabilityDayDto.getDayOfWeek());
        availabilityDay.setStatus(availabilityDayDto.getStatus());
        availabilityDay.setUserId(availabilityDayDto.getUserId());
        return availabilityDay;
    }

    private void setAvailabilityDayIdIfPresent(AvailabilityDayDto availabilityDayDto, AvailabilityDay availabilityDay){
        if(availabilityDayDto.getId() != null){
            availabilityDay.setId(availabilityDayDto.getId());
        }
    }

}
