package com.capgemini.jstk.boardgamecapmates.restService;

import com.capgemini.jstk.boardgamecapmates.commons.IdIsNotANumberException;
import com.capgemini.jstk.boardgamecapmates.commons.UserNotFoundException;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.UserDto;
import com.capgemini.jstk.boardgamecapmates.service.impl.ManageUserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserDataController {

    private ManageUserProfileService manageUserProfileService;

    @Autowired
    public UserDataController(ManageUserProfileService manageUserProfileService) {
        this.manageUserProfileService = manageUserProfileService;
    }

    @GetMapping("/get")
    public UserDto user(@RequestParam("userId") String userId) throws UserNotFoundException {
        Long id = validateId(userId);
        return manageUserProfileService.getUserDataById(id);
    }

    @DeleteMapping("/delete")
    public boolean delete(@RequestBody UserDto userDto) throws UserNotFoundException{
        return manageUserProfileService.removeUser(userDto);
    }

    private Long validateId(String stringId){
        try {
            return Long.valueOf(stringId);
        }
        catch (Exception e){
            throw new IdIsNotANumberException(stringId);
        }
    }

}
