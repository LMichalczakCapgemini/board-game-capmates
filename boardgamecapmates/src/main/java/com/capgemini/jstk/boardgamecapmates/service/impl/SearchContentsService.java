package com.capgemini.jstk.boardgamecapmates.service.impl;

import com.capgemini.jstk.boardgamecapmates.dao.impl.ContestDao;
import com.capgemini.jstk.boardgamecapmates.mapper.ContestMapper;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.ContestDto;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.SearchContestCriteria;
import com.capgemini.jstk.boardgamecapmates.service.ISearchContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class SearchContentsService implements ISearchContentService {

    private ContestDao contestDao;
    private ContestMapper contestMapper;

    @Autowired
    public SearchContentsService(ContestDao contestDao, ContestMapper contestMapper) {
        this.contestDao = contestDao;
        this.contestMapper = contestMapper;
    }

    public List<ContestDto> filterSearchContests(SearchContestCriteria searchContestCriteria){
        List<Optional<ContestDto>> optionalFinalSearch =  contestMapper.map(contestDao.findAllContests());

        if(searchContestCriteria.getMemberUserDto() != null){
            List<Optional<ContestDto>> userSearch = contestMapper.map(contestDao.findByUserId(searchContestCriteria.getMemberUserDto().getId()));
            optionalFinalSearch.retainAll(userSearch);
        }

        if(searchContestCriteria.getBoardGameDto() != null){
            List<Optional<ContestDto>> boardGameSearch = contestMapper.map(contestDao.findByBoardGameId(searchContestCriteria.getBoardGameDto().getId()));
            optionalFinalSearch.retainAll(boardGameSearch);
        }

        if(searchContestCriteria.getWinnerUserDto() != null){
            List<Optional<ContestDto>> winnerSearch = contestMapper.map(contestDao.findByUserId(searchContestCriteria.getWinnerUserDto().getId()));
            optionalFinalSearch.retainAll(winnerSearch);
        }

        List<ContestDto> finalSearch = new LinkedList<>();
        optionalFinalSearch.stream().forEach(contestDtoOptional -> {
            if(contestDtoOptional.isPresent())
                finalSearch.add(contestDtoOptional.get());
        });

        return finalSearch;
    }
}