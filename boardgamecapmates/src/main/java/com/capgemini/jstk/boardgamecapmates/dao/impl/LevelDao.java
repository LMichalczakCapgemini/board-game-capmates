package com.capgemini.jstk.boardgamecapmates.dao.impl;

import com.capgemini.jstk.boardgamecapmates.dao.ILevelDao;
import com.capgemini.jstk.boardgamecapmates.entity.Level;
import com.capgemini.jstk.boardgamecapmates.enums.LevelType;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class LevelDao implements ILevelDao {
    private static List<Level> levels;

    static {
        resetLevels();
    }

    private static void resetLevels(){
        levels = new ArrayList<>();
        levels.add(new Level(LevelType.Beginner));
        levels.add(new Level(LevelType.Advanced));
        levels.add(new Level(LevelType.Master));
    }

    @Override
    public Optional<Level> findById(Long id) {
        return levels.stream().filter(l -> l.getId().equals(id)).findFirst();
    }
}
