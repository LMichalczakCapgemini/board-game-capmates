package com.capgemini.jstk.boardgamecapmates.service.validator.impl;

import com.capgemini.jstk.boardgamecapmates.commons.EmptyGemeDesctiptionException;
import com.capgemini.jstk.boardgamecapmates.commons.EmptyNameException;
import com.capgemini.jstk.boardgamecapmates.mapper.dto.BoardGameDto;
import com.capgemini.jstk.boardgamecapmates.service.validator.IBoardGameValidator;

public class BoardGameValidator implements IBoardGameValidator {

    @Override
    public void validate(BoardGameDto boardGameDto) {
        if(boardGameDto.getName().isEmpty()){
            throw new EmptyNameException();
        }
        if(boardGameDto.getDescription().isEmpty()){
            throw new EmptyGemeDesctiptionException();
        }
    }
}
